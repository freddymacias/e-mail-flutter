import 'package:flutter/material.dart';
import '../model/email.dart';


class DetailScreen extends StatelessWidget {
  Email email;
  DetailScreen({Key? key, required this.email}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(email.subject),
        backgroundColor: Colors.blue,
        centerTitle: true,
      ),
      body: Column(children: [
        Row(mainAxisAlignment: MainAxisAlignment.start, children: [
          Padding(
            padding: EdgeInsets.only(top: 15, left: 10),
            child: Text(
              "From: ${email.from}",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 15,
              ),
            ),
          ),
        ]),
        Divider(
          color: Colors.blue,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
                padding: EdgeInsets.only(left: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(email.subject),
                  ],
                )),
            Row(
              children: [
                Padding(
                  padding: EdgeInsets.only(left: 10, right: 10),
                  child: Text(email.dateTime.toString(),
                      style: TextStyle(fontSize: 10)),
                ),
              ],
            )
          ],
        ),
        Divider(
          color: Colors.blue,
        ),
        Padding(
          padding: EdgeInsets.only(top: 3, left: 10, right: 10),
          child: Row(
            children: [
              Flexible(
                flex: 1,
                child: Text(
                  email.body,
                  textAlign: TextAlign.justify,
                ),
              ),
            ],
          ),
        )
      ]),
    );
  }
}
