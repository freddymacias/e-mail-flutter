import 'package:flutter/material.dart';
import '../model/backend.dart';
import '../model/email.dart';
import 'DetailScreen.dart';

class ListScreen extends StatefulWidget {
  @override
  _ListScreenState createState() => _ListScreenState();
}

class _ListScreenState extends State<ListScreen> {
  List<Email> emails = Backend().getEmails();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('APP MAIL'),
          centerTitle: true,
        ),
        body: ListView.builder(
            itemCount: emails.length,
            itemBuilder: (BuildContext context, int index) {
              final email = emails[index];
              final id = email.id;
              final bool read = email.read;
              return Dismissible(
                key: ValueKey(id),
                direction: DismissDirection.endToStart,
                background: Container(
                  color: Colors.blue,

                  child: Icon(
                    Icons.delete_forever_sharp,
                    color: Colors.black,
                    size: 40,
                  ),
                ),
                
                child: Container(
                  child: Column(
                    children: [
                      Text(
                        email.dateTime.toString(),
                        style: TextStyle(color: Colors.black, fontSize: 10),
                      ),
                      ListTile(
                        
                        title: Row(
                          //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            
                            Text(
                              email.from,
                              style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 13),
                            ),
                          ],                         
                        ), 
                                              
                        subtitle: Container(
                          padding: const EdgeInsets.only(top: 5.0),
                          child: Row(
                              //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                 Icon(
                                    read
                                        ? Icons.brightness_1_outlined
                                        : Icons.brightness_1,
                                    color: Colors.purple,
                                ),
                                Text(email.subject,
                                    style: TextStyle(
                                      fontSize: 15,
                                    )), 
                              ]),
                        ),
                        onLongPress: () {
                          Backend().markEmailAsRead(id);
                          setState(() {});
                        },
                        onTap: () {
                          Backend().markEmailAsRead(id);
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => DetailScreen(
                                    email: emails[index],
                                  )));
                          setState(() {});
                        },
                      ),
                      Divider(color: Colors.black),
                    ],
                  ),
                ),
                onDismissed: (direction) {
                  setState(() {
                    emails.removeAt(index);
                    Backend().deleteEmail(id);
                    print(toString());
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        duration: Duration(seconds: 1),
                        content: Text( "$id")));
                    setState(() {});
                  });
                },
              );
            }));
  }
}
